/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { PullToRefreshV2 } from '@ohos/pulltorefresh'

@Entry
@ComponentV2
struct Index {
  private dataNumbers: Array<ResourceStr> = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
  private dataStrings: Array<ResourceStr> =
    [$r("app.string.MyComments"), $r("app.string.RelatedToMe"), $r("app.string.PersonalCenter1"),
      $r("app.string.PersonalCenter2"), $r("app.string.PersonalCenter3"), $r("app.string.MyReleases"),
      $r("app.string.Settings"), $r("app.string.LogOut")];
  @Local data: Array<ResourceStr> = this.dataStrings;
  // 需绑定列表或宫格组件
  private scroller: Scroller = new Scroller();

  private getResourceString(res: Resource) {
    return getContext().resourceManager.getStringSync(res.id)
  }

  build() {
    Column() {
      PullToRefreshV2({
        // 可选项，列表组件所绑定的数据
        data: this.data,
        // 必传项，需绑定传入主体布局内的列表或宫格组件
        scroller: this.scroller,
        // 必传项，自定义主体布局，内部有列表或宫格组件
        customList: () => {
          // 一个用@Builder修饰过的UI方法
          this.getListView();
        },
        // 可选项，下拉刷新回调
        onRefresh: () => {
          return new Promise<string>((resolve, reject) => {
            // 模拟网络请求操作，请求网络2秒后得到数据，通知组件，变更列表数据
            setTimeout(() => {
              resolve(this.getResourceString($r("app.string.RefreshSuccessful")));
              this.data = [...this.dataNumbers];
            }, 2000);
          });
        },
        // 可选项，上拉加载更多回调
        onLoadMore: () => {
          return new Promise<string>((resolve, reject) => {
            // 模拟网络请求操作，请求网络2秒后得到数据，通知组件，变更列表数据
            setTimeout(() => {
              resolve('');
              this.data.push(`${this.getResourceString($r("app.string.AddedEntry"))} ${this.data.length}`);
            }, 2000);
          });
        },
        customLoad: null,
        customRefresh: null,
      })
    }
  }

  @Builder
  private getListView() {
    List({ space: 0, scroller: this.scroller }) {
      ForEach(this.data, (item: string) => {
        ListItem() {
          Text(item)
            .fontSize(20)
        }
        .width('100%')
        .height('100%')
        .backgroundColor('#95efd2')
      })
    }
    .backgroundColor('#eeeeee')
    .divider({ strokeWidth: 1, color: 0x222222 })
    .edgeEffect(EdgeEffect.None) // 必须设置列表为滑动到边缘无效果
    .scrollSnapAlign(ScrollSnapAlign.CENTER)
  }
}